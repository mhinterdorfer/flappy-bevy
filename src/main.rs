use std::f32::consts::PI;
use std::time::Duration;

use bevy::math::bounding::{Aabb2d, BoundingCircle, IntersectsVolume};
use bevy::prelude::*;
use bevy::time::common_conditions::on_timer;
use bevy::time::Stopwatch;
use bevy::window::WindowTheme;
use iyes_perf_ui::{PerfUiCompleteBundle, PerfUiPlugin};
use rand::Rng;

#[derive(Component)]
struct Bird;

#[derive(Component)]
struct Pipe;

#[derive(Component)]
struct Velocity {
    velocity: Vec3,
}

#[derive(Component)]
struct Acceleration {
    acceleration: Vec3,
}

#[derive(Component)]
struct MenuText;

#[derive(Resource)]
struct Score {
    time: Stopwatch,
    points: usize,
}

#[derive(Component)]
struct Point;

#[derive(States, Default, Debug, Clone, PartialEq, Eq, Hash)]
enum AppState {
    #[default]
    Running,
    Dead,
}

fn main() {
    let mut app = App::new();
    app.add_plugins(DefaultPlugins.set(WindowPlugin {
        primary_window: Some(Window {
            title: "Flappy Bevy".into(),
            name: Some("flappy-bevy.app".into()),
            resolution: (720.0, 480.0).into(),
            prevent_default_event_handling: false,
            window_theme: Some(WindowTheme::Dark),
            resizable: false,
            enabled_buttons: bevy::window::EnabledButtons {
                maximize: false,
                ..Default::default()
            },
            ..default()
        }),
        ..default()
    }))
    .init_state::<AppState>()
    .add_systems(Startup, setup_system)
    .add_systems(
        Update,
        (
            (acceleration_system, velocity_system, rotating_bevy_system).chain(),
            user_input_system,
            bevy_pipe_collision_system,
            death_system,
            pipe_despawning_system,
        )
            .run_if(in_state(AppState::Running)),
    )
    .add_systems(
        Update,
        (pipe_spawning_system)
            .run_if(in_state(AppState::Running))
            .run_if(on_timer(Duration::from_secs(2))),
    )
    .add_systems(OnEnter(AppState::Dead), setup_died_system)
    .add_systems(OnEnter(AppState::Running), setup_running_system)
    .add_systems(Update, (respawn_system,).run_if(in_state(AppState::Dead)));
    #[cfg(debug_assertions)]
    {
        app.add_plugins(bevy::diagnostic::FrameTimeDiagnosticsPlugin)
            .add_plugins(bevy::diagnostic::EntityCountDiagnosticsPlugin)
            .add_plugins(bevy::diagnostic::SystemInformationDiagnosticsPlugin)
            .add_plugins(PerfUiPlugin);
    }
    app.run()
}

fn rotating_bevy_system(mut bevy_q: Query<(&mut Transform, &Velocity), With<Bird>>){
    if let (mut transform, velocity) = bevy_q.single_mut(){
        if velocity.velocity.y > 100.0 {
            transform.rotation = Quat::from_rotation_z(0.785)
        }
        else if velocity.velocity.y < -100.0 {
            transform.rotation = Quat::from_rotation_z(-0.785)
        }else{
            transform.rotation = Quat::from_rotation_z(0.0)
        }
    }
}

fn setup_died_system(mut commands: Commands) {
    commands.spawn((
        // Create a TextBundle that has a Text with a single section.
        TextBundle::from_sections([
            TextSection::new(
                "YOU DIED!",
                TextStyle {
                    // This font is loaded and will be used instead of the default font.
                    font_size: 70.0,
                    color: Color::RED,
                    ..default()
                },
            ),
            TextSection::new(
                "\nPress space to continue...",
                TextStyle {
                    font_size: 35.0,
                    color: Color::BLUE,
                    ..default()
                },
            ),
        ]) // Set the justification of the Text
        .with_text_justify(JustifyText::Center)
        // Set the style of the TextBundle itself.
        .with_style(Style {
            position_type: PositionType::Absolute,
            bottom: Val::Percent(40.0),
            right: Val::Percent(20.0),
            ..default()
        }),
        MenuText,
    ));
}

fn setup_running_system(
    mut commands: Commands,
    old_entites_q: Query<(Entity), Or<(With<MenuText>, With<Bird>, With<Pipe>)>>,
    asset_server: Res<AssetServer>,
) {
    // despawn old entities
    old_entites_q
        .iter()
        .for_each(|entity: Entity| commands.entity(entity).despawn());

    commands.spawn((
        Bird,
        SpriteBundle {
            texture: asset_server.load("sprites/bevy_bird_dark.png"),
            sprite: Sprite {
                custom_size: Some(Vec2::new(48.0, 48.0)),
                ..default()
            },
            transform: Transform::from_translation(Vec3::new(-250.0, 0.0, 50.0)),
            ..default()
        },
        Velocity {
            velocity: Vec3::ZERO,
        },
        Acceleration {
            acceleration: Vec3::new(0.0, -150.0, 0.0),
        },
    ));

    commands.insert_resource(Score {
        time: Stopwatch::new(),
        points: 0,
    });

    commands.spawn((
        TextBundle::from_sections([
            TextSection::new(
                "Score: ",
                TextStyle {
                    font_size: 24.0,
                    ..default()
                },
            ),
            TextSection::new(
                "0",
                TextStyle {
                    font_size: 24.0,
                    ..default()
                },
            ),
        ]) // Set the justification of the Text
        .with_text_justify(JustifyText::Left)
        // Set the style of the TextBundle itself.
        .with_style(Style {
            position_type: PositionType::Absolute,
            left: Val::Px(0.0),
            top: Val::Px(0.0),
            ..default()
        }),
        MenuText,
    ));
}

fn setup_system(mut commands: Commands) {
    // Camera
    commands.spawn(Camera2dBundle::default());
    #[cfg(debug_assertions)]
    {
        commands.spawn((PerfUiCompleteBundle::default()));
    }
}

fn velocity_system(
    mut entites_q: Query<(&mut Transform, &Velocity)>,
    time: Res<Time>,
    score: Res<Score>,
) {
    entites_q.iter_mut().for_each(|(mut transform, velocity)| {
        transform.translation +=
            velocity.velocity * (time.delta_seconds() + score.time.elapsed_secs() * 0.001)
    })
}
fn acceleration_system(mut entites_q: Query<(&mut Velocity, &Acceleration)>, time: Res<Time>) {
    entites_q
        .iter_mut()
        .for_each(|(mut velocity, acceleration)| {
            velocity.velocity += acceleration.acceleration * time.delta_seconds()
        })
}

fn user_input_system(
    keys: Res<ButtonInput<KeyCode>>,
    mut bird_q: Query<(&mut Velocity), (With<Bird>)>,
) {
    if keys.just_released(KeyCode::Space) {
        if let (mut velocity) = bird_q.single_mut() {
            velocity.velocity = Vec3::new(velocity.velocity.x, 150.0, velocity.velocity.z)
        }
    }
}
fn respawn_system(keys: Res<ButtonInput<KeyCode>>, mut next_state: ResMut<NextState<AppState>>) {
    if keys.just_released(KeyCode::Space) {
        next_state.set(AppState::Running)
    }
}

fn death_system(
    bird_q: Query<(&Transform), (With<Bird>)>,
    mut next_state: ResMut<NextState<AppState>>,
) {
    if let (transform) = bird_q.single() {
        if transform.translation.y < -240.0 || transform.translation.y > 240.0 {
            next_state.set(AppState::Dead)
        }
    }
}

fn bevy_pipe_collision_system(
    bird_q: Query<(&Transform), (With<Bird>)>,
    pipe_q: Query<(&Transform), (With<Pipe>)>,
    mut next_state: ResMut<NextState<AppState>>,
) {
    let bird_bound = BoundingCircle::new(bird_q.get_single().unwrap().translation.truncate(), 20.0);
    for (pipe_transform) in pipe_q.iter() {
        let pipe_bound = Aabb2d::new(
            pipe_transform.translation.truncate(),
            Vec2::new(40.0 / 2.0, 270.0 / 2.0),
        );
        if bird_bound.intersects(&pipe_bound) {
            next_state.set(AppState::Dead)
        }
    }
}

fn pipe_despawning_system(
    mut commands: Commands,
    pipe_e_q: Query<(Entity, &Transform), With<Point>>,
    mut score: ResMut<Score>,
    mut score_text: Query<&mut Text, With<MenuText>>,
) {
    for (pipe_e, pipe_transform) in pipe_e_q.iter() {
        if pipe_transform.translation.x < -360.0 {
            commands.entity(pipe_e).despawn();
            score.points += 1;
        }
    }
    if let (mut score_text) = score_text.single_mut() {
        score_text.sections[1].value = format!("{}", score.points);
    }
}

fn pipe_spawning_system(mut commands: Commands, asset_server: Res<AssetServer>) {
    let mut rng = rand::thread_rng();
    let y_offset = rng.gen_range(-50.0..50.0);
    let x_offset = rng.gen_range(-20.0..20.0);
    commands.spawn((
        Pipe,
        SpriteBundle {
            texture: asset_server.load("sprites/pipe.png"),
            sprite: Sprite {
                custom_size: Some(Vec2::new(50.0, 300.0)),
                ..default()
            },
            transform: Transform::from_translation(Vec3::new(
                720.0 + x_offset,
                -200.0 + y_offset,
                0.0,
            )),
            ..default()
        },
        Velocity {
            velocity: Vec3::new(-100.0, 0.0, 0.0),
        },
    ));
    commands.spawn((
        Pipe,
        SpriteBundle {
            texture: asset_server.load("sprites/pipe.png"),
            sprite: Sprite {
                custom_size: Some(Vec2::new(50.0, 300.0)),
                ..default()
            },
            transform: Transform::from_translation(Vec3::new(
                720.0 + x_offset,
                200.0 + y_offset,
                0.0,
            ))
            .with_rotation(Quat::from_rotation_x(PI)),
            ..default()
        },
        Velocity {
            velocity: Vec3::new(-100.0, 0.0, 0.0),
        },
        Point,
    ));
}
